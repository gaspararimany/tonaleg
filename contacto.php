<?php
include_once("layout/header.php");
include_once("layout/nav.php");
?>



<div class="container-fluid alfa"> 

<!--Section: Contact v.1-->
<section class="section mb-4">

    <!--Section heading-->
    <h1 class="section-heading">Contacto</h1>
    <!--Section sescription-->
    <p class="section-description mb-5">Tonaleg cuenta con Atencion al cliente personalizada por distintos medios de comunicacion para gestionar pedidos de forma rapida y segura.</p>

    <div class="row">

        <!--First column-->
        <div class="col-md-5">

            <!--Form with header-->
            <div class="card">

                <div class="card-block">
                    <!--Header-->
                    <div class="form-header mdb-color">
                        <h3><i class="fa fa-envelope"></i> Escríbenos:</h3>
                    </div>

                    <p>Te responderemos a la brevedad</p>
                    <br>

                    <!--Body-->
                    <div class="md-form">
                        <i class="fa fa-user prefix"></i>
                        <input type="text" id="form3" class="form-control">
                        <label for="form3">Tu Nombre</label>
                    </div>

                    <div class="md-form">
                        <i class="fa fa-envelope prefix"></i>
                        <input type="text" id="form2" class="form-control">
                        <label for="form2">Tu Email</label></label>
                    </div>

                    <div class="md-form">
                        <i class="fa fa-tag prefix"></i>
                        <input type="text" id="form32" class="form-control">
                        <label for="form32">Asunto</label>
                    </div>

                    <div class="md-form">
                        <i class="fa fa-pencil prefix"></i>
                        <textarea type="text" id="form8" class="md-textarea"></textarea>
                        <label for="form8">Mensaje</label>
                    </div>

                    <div class="text-center">
                        <button class="btn btn-ins">Enviar</button>
                    </div>

                </div>

            </div>
            <!--/Form with header-->

        </div>
        <!--/First column-->

        <!--Second column-->
        <div class="col-md-7">

            <!--Google map-->
            <div id="map-container" class="z-depth-1-half map-container" style="height: 400px"></div>

            <br>
            <!--Buttons-->
            <div class="row text-center">
                <div class="col-md-4">
                    <a class="btn-floating btn-small mdb-color"><i class="fa fa-map-marker"></i></a>
                    <p>Buenos Aires</p>
                    <p>Argentina</p>
                </div>

                <div class="col-md-4">
                    <a class="btn-floating btn-small mdb-color"><i class="fa fa-phone"></i></a>
                    <p>+ 341555666</p>
                    <p>Lunes - Viernes, 8:00-22:00</p>
                </div>

                <div class="col-md-4">
                    <a class="btn-floating btn-small mdb-color"><i class="fa fa-envelope"></i></a>
                    <p>tonaleg@gmail.com</p>
                    <p>tonale@gmail.com</p>
                </div>
            </div>

        </div>
        <!--/Second column-->

    </div>
    
</div>

</section>
<!--/Section: Contact v.1-->

<script type="text/javascript" >
function init_map() {
    
    var var_location = new google.maps.LatLng(-32.947818, -60.630494);

    var var_mapoptions = {
        center: var_location,
    
        zoom: 14
    };

    var var_marker = new google.maps.Marker({
        position: var_location,
        map: var_map,
        title: "Rosario"
    });

    var var_map = new google.maps.Map(document.getElementById("map-container"),
        var_mapoptions);

    var_marker.setMap(var_map);

}

google.maps.event.addDomListener(window, 'load', init_map);

</script>
<?php
include_once("layout/footer.php");
?>