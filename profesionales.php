<?php
    include_once("layout/header.php");
    include_once("layout/nav.php");
?>

<div class="container-fluid"> 

<!--Main parallax wrapper-->
<div class="parallax alfa">

    <!--First section-->
    <div id="section-1" class="parallax-section ">

        <!--Parallax content-->
        <div class="parallax-layer parallax-layer-base ">

            <!--Container to center the content-->
            <div class="full-bg-img flex-center">
                <ul>
                    <li>
                        <h3 class="h1-responsive  fadeInDown" data--delay="0.2s"> PRODUCTOS POR CATEGORIAS </h3>
                    </li>
                </ul>
            </div>
            <!--/Container to center the content-->
        </div>
        <!--/Parallax content-->

        <!--Parallax background-->
        <div class="parallax-layer parallax-layer-back">
        </div>
        <!--/Parallax background-->
    </div>
    <!--/First section-->
    <!--Dummy Content-->
    <div id="section-2">
        <div class="container">    
            <div class="row text-center">
                <div class="col-md-3 cardProductos  ">
                    <!--Card-->
                    <div class="card">
                        <!--Card image-->
                        <img class="img-fluid img-responsive" src="/img/pictures/shampoo1.jpeg" alt="Card image cap">
                        <!--/.Card image-->
                    
                        <!--Card content-->
                        <div class="card-block">
                            <!--Title-->
                            <h4 class="card-title">SHAMPOO</h4>
                            <a href="shampoo.php" class="btn boton-pofesional">Ver Todos</a>
                        </div>
                        <!--/.Card content-->
                    </div>
                    <!--/.Card-->
                </div>
                <div class="col-md-3 cardProductos  ">
                    <!--Card-->
                    <div class="card">
                        <!--Card image-->
                        <img class="img-fluid img-responsive" src="/img/pictures/shampoo1.jpeg" alt="Card image cap">
                        <!--/.Card image-->
                    
                        <!--Card content-->
                        <div class="card-block">
                            <!--Title-->
                            <h4 class="card-title">ALISADOS</h4>
                            <a href="#" class="btn boton-pofesional">Ver Todos</a>
                        </div>
                        <!--/.Card content-->
                    </div>
                    <!--/.Card-->
                </div>
                <div class="col-md-3 cardProductos  ">
                    <!--Card-->
                    <div class="card">
                        <!--Card image-->
                        <img class="img-fluid img-responsive" src="/img/pictures/shampoo1.jpeg" alt="Card image cap">
                        <!--/.Card image-->
                    
                        <!--Card content-->
                        <div class="card-block">
                            <!--Title-->
                            <h4 class="card-title">CREMA ENJUAGE</h4>
                            <a href="#" class="btn boton-pofesional">Ver Todos</a>
                        </div>
                        <!--/.Card content-->
                    </div>
                    <!--/.Card-->
                </div>
                <div class="col-md-3 cardProductos  ">
                    <!--Card-->
                    <div class="card">
                        <!--Card image-->
                        <img class="img-fluid img-responsive" src="/img/pictures/gel.png" alt="Card image cap">
                        <!--/.Card image-->
                    
                        <!--Card content-->
                        <div class="card-block">
                            <!--Title-->
                            <h4 class="card-title">GELES</h4>
                            <a href="#" class="btn boton-pofesional">Ver Todos</a>
                        </div>
                        <!--/.Card content-->
                    </div>
                    <!--/.Card-->
                </div>
                <div class="col-md-3 cardProductos  ">
                    <!--Card-->
                    <div class="card">
                        <!--Card image-->
                        <img class="img-fluid img-responsive" src="/img/pictures/shampoo1.jpeg" alt="Card image cap">
                        <!--/.Card image-->
                    
                        <!--Card content-->
                        <div class="card-block">
                            <!--Title-->
                            <h4 class="card-title">CREMA ENJUAGE</h4>
                            <a href="#" class="btn boton-pofesional">Ver Todos</a>
                        </div>
                        <!--/.Card content-->
                    </div>
                    <!--/.Card-->
                </div>
                <div class="col-md-3 cardProductos  ">
                    <!--Card-->
                    <div class="card">
                        <!--Card image-->
                        <img class="img-fluid img-responsive" src="/img/pictures/shampoo1.jpeg" alt="Card image cap">
                        <!--/.Card image-->
                    
                        <!--Card content-->
                        <div class="card-block">
                            <!--Title-->
                            <h4 class="card-title">CREMA ENJUAGE</h4>
                            <a href="#" class="btn boton-pofesional">Ver Todos</a>
                        </div>
                        <!--/.Card content-->
                    </div>
                    <!--/.Card-->
                </div>
                <div class="col-md-3 cardProductos  ">
                    <!--Card-->
                    <div class="card">
                        <!--Card image-->
                        <img class="img-fluid img-responsive" src="/img/pictures/shampoo1.jpeg" alt="Card image cap">
                        <!--/.Card image-->
                    
                        <!--Card content-->
                        <div class="card-block">
                            <!--Title-->
                            <h4 class="card-title">CREMA ENJUAGE</h4>
                            <a href="#" class="btn boton-pofesional">Ver Todos</a>
                        </div>
                        <!--/.Card content-->
                    </div>
                    <!--/.Card-->
                </div>
                <div class="col-md-3 cardProductos  ">
                    <!--Card-->
                    <div class="card">
                        <!--Card image-->
                        <img class="img-fluid img-responsive" src="/img/pictures/shampoo1.jpeg" alt="Card image cap">
                        <!--/.Card image-->
                    
                        <!--Card content-->
                        <div class="card-block">
                            <!--Title-->
                            <h4 class="card-title">GELES</h4>
                            <a href="#" class="btn boton-pofesional">Ver Todos</a>
                        </div>
                        <!--/.Card content-->
                    </div>
                    <!--/.Card-->
                </div>
                <div class="col-md-3 cardProductos  ">
                    <!--Card-->
                    <div class="card">
                        <!--Card image-->
                        <img class="img-fluid img-responsive" src="/img/pictures/shampoo1.jpeg" alt="Card image cap">
                        <!--/.Card image-->
                    
                        <!--Card content-->
                        <div class="card-block">
                            <!--Title-->
                            <h4 class="card-title">MILK</h4>
                            <a href="#" class="btn boton-pofesional">Ver Todos</a>
                        </div>
                        <!--/.Card content-->
                    </div>
                    <!--/.Card-->
                </div>
                <div class="col-md-3 cardProductos  ">
                    <!--Card-->
                    <div class="card">
                        <!--Card image-->
                        <img class="img-fluid img-responsive" src="/img/pictures/shampoo1.jpeg" alt="Card image cap">
                        <!--/.Card image-->
                    
                        <!--Card content-->
                        <div class="card-block">
                            <!--Title-->
                            <h4 class="card-title">CAPILARES</h4>
                            <a href="#" class="btn boton-pofesional">Ver Todos</a>
                        </div>
                        <!--/.Card content-->
                    </div>
                    <!--/.Card-->
                </div>
                <div class="col-md-3 cardProductos  ">
                    <!--Card-->
                    <div class="card">
                        <!--Card image-->
                        <img class="img-fluid img-responsive" src="/img/pictures/shampoo1.jpeg" alt="Card image cap">
                        <!--/.Card image-->
                    
                        <!--Card content-->
                        <div class="card-block">
                            <!--Title-->
                            <h4 class="card-title">RULOS</h4>
                            <a href="#" class="btn boton-pofesional">Ver Todos</a>
                        </div>
                        <!--/.Card content-->
                    </div>
                    <!--/.Card-->
                </div>
                <div class="col-md-3 cardProductos  ">
                    <!--Card-->
                    <div class="card">
                        <!--Card image-->
                        <img class="img-fluid img-responsive" src="/img/pictures/shampoo1.jpeg" alt="Card image cap">
                        <!--/.Card image-->
                    
                        <!--Card content-->
                        <div class="card-block">
                            <!--Title-->
                            <h4 class="card-title">PERMANENTES</h4>
                            <a href="#" class="btn boton-pofesional">Ver Todos</a>
                        </div>
                        <!--/.Card content-->
                    </div>
                    <!--/.Card-->
                </div>
                
            </div>
        </div>
    </div>
    <!--Dummy Content-->

</div>

<script >

    
        $(".boton-pofesional").hover(function () {
            $(this).siblings().removeClass('boton-pofesional');
            $(this).addClass('boton-pofesional-active');
        }
        , function () {
    $(this).removeClass('boton-pofesional-active');
    $(this).addClass('boton-pofesional');
});
        
    
    
</script>
<!--/Main parallax wrapper-->
<?php
include_once("layout/footer.php");
?>