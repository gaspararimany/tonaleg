<!--Navbar-->
    <nav class="navbar navbar-toggleable-sm scrolling-navbar white fixed-top ">
        <div class="container">
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNav1" aria-controls="navbarNav1" aria-expanded="false" aria-label="Toggle navigation">
                <span class="fa fa-bars" aria-hidden="true"></span>
            </button>
            <a href="index.php" class="navbar-brand" >
                <strong>Tonaleg</strong>
            </a>
            <div class="collapse navbar-collapse" id="navbarNav1">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item btn-group dropdown">
                    <a class="nav-link dropdown-toggle" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">PRODUCTOS</a>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenu1">
                        <a href="profesionales.php" class="dropdown-item"> Profesionales</a>
                        <a class="dropdown-item">Coloracion</a>
                    </div>
                </li>
                    <li class="nav-item">
                        <a href="contacto.php" class="nav-link">CONTACTO</a>
                    </li>
                    <li class="nav-item">
                        <a href="empresa.php" class="nav-link">NUESTRA EMPRESA</a>
                    </li>
                </ul>
                
            </div>
        </div>
    </nav>
    <!--/.Navbar-->