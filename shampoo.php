
<?php
include_once("layout/header.php");
include_once("layout/nav.php");
?>

<div class="container-fluid"> 

    <!--Main parallax wrapper-->
    <div class="parallax">

        <!--First section-->
        <div id="section-1-shampoo" class="parallax-section">

            <!--Parallax content-->
            <div class="parallax-layer parallax-layer-base">

                <!--Container to center the content-->
                <div class="full-bg-img flex-center">
                    <ul>
                        <li>
                            <h3 class="h1-responsive  fadeInDown" data--delay="0.2s"> SHAMPOO </h3>
                        </li>
                    </ul>
                </div>
                <!--/Container to center the content-->
            </div>
            <!--/Parallax content-->

            <!--Parallax background-->
            <div class="parallax-layer parallax-layer-back">
            </div>
            <!--/Parallax background-->
        </div>
        <!--/First section-->
        <!--Dummy Content-->
        <div id="section-2">
            <div class="container producto">
                
                <!-- Shampoo 1 -->
                <div class="row producto hoverable">
                    <div class="col-md-6 view hm-zoom">
                        <img class="img-responsive mx-auto d-block" src="/img/pictures/shampoo1.jpeg">
                    </div>
                    <div class="col-md-6">
                        <h1 class="heading primary h1-responsive">ORTIGA</h1>
                        <h4 >SHAMPO AROMATICO CON EXTRACTO DE ORTIGA</h4>
                        <!-- 2 tabs -->
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs tabs-2 black" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#panel1-1" role="tab">Información</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#panel2-1" role="tab">Detalles</a>
                            </li>
                        </ul>
                        <!-- Tab panels -->
                        <div class="tab-content card">

                            <!--Panel 1-->
                            <div class="tab-pane fade in show active" id="panel1-1" role="tabpanel">
                                
                                <p>
                                    <span class="heading"> Que Hace?</span>
                                    <br>
                                    Ayuda a mejorar el color del cabello
                                </p>
                                <p>
                                    <span class="heading"> Como funciona?</span>
                                    <br> 
                                    Cubre el cabello con su formula magica
                                </p>
                                <p>
                                    <span class="heading"> Presentacion</span>
                                    <br> 
                                    150cc
                                </p> 
                            </div>
                            <!--/.Panel 1-->

                            <!--Panel 2-->
                            <div class="tab-pane fade" id="panel2-1" role="tabpanel">
                                <p>
                                    <span class="heading">Como Aplicarlo?</span>
                                    <br>
                                    Moja el cabello.<br>
                                    Échate una nuez de champú en la mano y aplícatelo en el cabello.<br>
                                    Añade un poco de agua y masajea con suavidad. ...<br>
                                    Aclara y vuelve a aplicar una gota de champú para lavar bien el cabello por segunda vez.<br>
                                </p>
                            </div>
                            <!--/.Panel 2-->
                        </div>
                    </div>
                </div>
                <!-- fin Shampoo 1 -->
                
                <!-- Shampoo 2 -->
                <div class="row producto hoverable">
                    <div class="col-md-6 ">
                        <h1 class="heading primary h1-responsive">Semi di lino</h1>
                        <h4 >SHAMPO AROMATICO CON EXTRACTO DE ORTIGA</h4>
                        <!-- 2 tabs -->
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs tabs-2 black" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#panel2-1" role="tab">Información</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#panel2-2" role="tab">Detalles</a>
                            </li>
                        </ul>
                        <!-- Tab panels -->
                        <div class="tab-content card">

                            <!--Panel 1-->
                            <div class="tab-pane fade in show active" id="panel2-1" role="tabpanel">
                                
                                <p>
                                    <span class="heading"> Que Hace?</span>
                                    <br>
                                    Ayuda a mejorar el color del cabello
                                </p>
                                <p>
                                    <span class="heading"> Como funciona?</span>
                                    <br> 
                                    Cubre el cabello con su formula magica
                                </p>
                                <p>
                                    <span class="heading"> Presentacion</span>
                                    <br> 
                                    150cc
                                </p> 
                            </div>
                            <!--/.Panel 1-->

                            <!--Panel 2-->
                            <div class="tab-pane fade" id="panel2-2" role="tabpanel">
                                <p>
                                    <span class="heading">Como Aplicarlo?</span>
                                    <br>
                                    Moja el cabello.<br>
                                    Échate una nuez de champú en la mano y aplícatelo en el cabello.<br>
                                    Añade un poco de agua y masajea con suavidad. ...<br>
                                    Aclara y vuelve a aplicar una gota de champú para lavar bien el cabello por segunda vez.<br>
                                </p>
                            </div>
                            <!--/.Panel 2-->
                        </div>
                    </div>
                    <div class="col-md-6 hm-zoom">
                        <img class="img-responsive mx-auto d-block" src="/img/pictures/shampoo2.jpeg">
                    </div>
                </div>
                <!-- fin shampoo2 -->
                
                <!-- Shampoo 3 -->
                <div class="row producto hoverable">
                    
                    <div class="col-md-6 view hm-zoom">
                        <img class="img-responsive mx-auto d-block" src="/img/pictures/shampoo1.jpeg">
                    </div>
                    <div class="col-md-6 ">
                        <h1 class="heading primary h1-responsive">ORTIGA</h1>
                        <h4 >SHAMPO AROMATICO CON EXTRACTO DE ORTIGA</h4>
                        <!-- 2 tabs -->
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs tabs-2 black" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#panel3-1" role="tab">Información</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#panel3-2" role="tab">Detalles</a>
                            </li>
                        </ul>
                        <!-- Tab panels -->
                        <div class="tab-content card">

                            <!--Panel 1-->
                            <div class="tab-pane fade in show active" id="panel3-1" role="tabpanel">
                                
                                <p>
                                    <span class="heading"> Que Hace?</span>
                                    <br>
                                    Ayuda a mejorar el color del cabello
                                </p>
                                <p>
                                    <span class="heading"> Como funciona?</span>
                                    <br> 
                                    Cubre el cabello con su formula magica
                                </p>
                                <p>
                                    <span class="heading"> Presentacion</span>
                                    <br> 
                                    150cc
                                </p> 
                            </div>
                            <!--/.Panel 1-->

                            <!--Panel 2-->
                            <div class="tab-pane fade" id="panel3-2" role="tabpanel">
                                <p>
                                    <span class="heading">Como Aplicarlo?</span>
                                    <br>
                                    Moja el cabello.<br>
                                    Échate una nuez de champú en la mano y aplícatelo en el cabello.<br>
                                    Añade un poco de agua y masajea con suavidad. ...<br>
                                    Aclara y vuelve a aplicar una gota de champú para lavar bien el cabello por segunda vez.<br>
                                </p>
                            </div>
                            <!--/.Panel 2-->
                        </div>
                    </div>
                </div>
                <!-- fin Shampoo 3 -->
                
                <!-- Shampoo 4 -->
                <div class="row producto hoverable">
                    <div class="col-md-6 ">
                        <h1 class="heading primary h1-responsive">Semi di lino</h1>
                        <h4 >SHAMPO AROMATICO CON EXTRACTO DE ORTIGA</h4>
                        <!-- 2 tabs -->
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs tabs-2 black" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#panel4-1" role="tab">Información</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#panel4-2" role="tab">Detalles</a>
                            </li>
                        </ul>
                        <!-- Tab panels -->
                        <div class="tab-content card">

                            <!--Panel 1-->
                            <div class="tab-pane fade in show active" id="panel4-1" role="tabpanel">
                                
                                <p>
                                    <span class="heading"> Que Hace?</span>
                                    <br>
                                    Ayuda a mejorar el color del cabello
                                </p>
                                <p>
                                    <span class="heading"> Como funciona?</span>
                                    <br> 
                                    Cubre el cabello con su formula magica
                                </p>
                                <p>
                                    <span class="heading"> Presentacion</span>
                                    <br> 
                                    150cc
                                </p> 
                            </div>
                            <!--/.Panel 1-->

                            <!--Panel 2-->
                            <div class="tab-pane fade" id="panel4-2" role="tabpanel">
                                <p>
                                    <span class="heading">Como Aplicarlo?</span>
                                    <br>
                                    Moja el cabello.<br>
                                    Échate una nuez de champú en la mano y aplícatelo en el cabello.<br>
                                    Añade un poco de agua y masajea con suavidad. ...<br>
                                    Aclara y vuelve a aplicar una gota de champú para lavar bien el cabello por segunda vez.<br>
                                </p>
                            </div>
                            <!--/.Panel 2-->
                        </div>
                    </div>
                    <div class="col-md-6 hm-zoom">
                        <img class="img-responsive mx-auto d-block" src="/img/pictures/shampoo2.jpeg">
                    </div>
                </div>
                <!-- fin shampoo4 -->
                
                
            </div>
        </div>
        <!--Dummy Content-->
    </div>
</div>
<!--/Main parallax wrapper-->
</div>
<?php
include_once("layout/footer.php");
?>