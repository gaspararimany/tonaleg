<?php
include_once("layout/header.php");
include_once("layout/nav.php");
?>

<div class="container-fluid ">
    <!--Main parallax wrapper-->
    

    <div class="row alfa">
        
            <!--Carousel Wrapper-->
            <div id="carousel-example-3" class="carousel slide carousel-fade white-text" data-ride="carousel" data-interval="false">
                <!--Indicators-->
                <ol class="carousel-indicators">
                    <li data-target="#carousel-example-3" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-example-3" data-slide-to="1"></li>
                    <li data-target="#carousel-example-3" data-slide-to="2"></li>
                </ol>
                <!--/.Indicators-->

                <!--Slides-->
                <div class="carousel-inner" role="listbox">

                    <!-- First slide -->
                    <div class="carousel-item active view hm-black-light" style="background-image: url('/img/pictures/girl1.jpg'); background-repeat: no-repeat; background-size: cover;">
                        
                        <!-- Caption -->
                        <div class="full-bg-img flex-center white-text">
                            <ul class="animated fadeIn col-md-12">
                                <li>
                                    <h1 class="h1-responsive">20 Photos to inspire you to visit Tatra Mountains</h1></li>
                                    <li>
                                        <p>Best places you should see, traditional dishes that you have to try</p>
                                    </li>
                                    <li>
                                        <a target="_blank" href="http://mdbootstrap.com/getting-started/" class="btn btn-info btn-lg" rel="nofollow">See more!</a>
                                    </li>
                                </ul>
                            </div>
                            <!-- /.Caption -->
                            
                        </div>
                        <!--/.First slide-->

                        <!-- Second slide -->
                        <div class="carousel-item view hm-black-light" style="background-image: url('/img/pictures/girl2.jpg'); background-repeat: no-repeat; background-size: cover;">
                            
                            <!-- Caption -->
                            <div class="full-bg-img flex-center white-text">
                                <ul class="animated fadeIn col-md-12">
                                    <li>
                                        <h1 class="h1-responsive">10 Reasons you should spend winter holiday in mountains </h1>
                                    </li>
                                    <li>
                                        <p>Best atractions and winter sports!</p>
                                    </li>
                                    <li>
                                        <a target="_blank" href="http://mdbootstrap.com/bootstrap-tutorial/" class="btn btn-info btn-lg" rel="nofollow">Read more</a>
                                    </li>
                                </ul>
                            </div>
                            <!-- /.Caption -->
                            
                        </div>
                        <!--/.Second slide -->

                        <!-- Third slide -->
                        <div class="carousel-item view hm-black-light" style="background-image: url('/img/pictures/girl4.jpg'); background-repeat: no-repeat; background-size: cover;">
                            
                            <!-- Caption -->
                            <div class="full-bg-img flex-center white-text">
                                <ul class="animated fadeIn col-md-12">
                                    <li>
                                        <h1 class="h1-responsive">Weekend in the nature - the best way to relax</h1></li>
                                        <li>
                                            <p>8 Reasons why you need to spend more time in nature</p>
                                        </li>
                                        <li>
                                            <a target="_blank" href="http://mdbootstrap.com/forums/forum/support/" class="btn btn-default btn-lg" rel="nofollow">Read more</a>
                                        </li>
                                    </ul>
                                </div>
                                <!-- /.Caption -->
                                
                            </div>
                            <!--/.Third slide-->
                        </div>
                        <!--/.Slides-->

                        <!--Controls-->
                        <a class="carousel-control-prev" href="#carousel-example-3" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carousel-example-3" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                        <!--/.Controls-->
                    </div>
                    <!--/.Carousel Wrapper-->
            </div>
                    <div class="row"> 
                        <section class="section">

                            <!--Section heading-->
                            <h1 class="section-heading wow fadeIn data-wow-delay="0.2s" "style="visibility: visible; animation-delay: 0.2s; animation-name: fadeIn; text-align: center"> NOVEDADES TONALEG </h1>
                            <!--Section sescription-->
                            <p class="section-description wow fadeIn data-wow-delay="0.4s"" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeIn;">texto informativo</p>

                            <!--First row-->
                            <div class="row">

                                <!--First column-->
                                <div class="col-lg-3 col-md-6 mb-r wow fadeIn" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeIn;">

                                    <!--Collection card-->
                                    <div class="card collection-card z-depth-1-half">
                                        <!--Card image-->
                                        <div class="view  hm-zoom">
                                            <img src="/img/pictures/m1.jpg" class="img-fluid" alt="">
                                            <div class="stripe dark white-text">
                                                <a>
                                                    <p> <h3>Reparacion Extrema</h3></p>
                                                </a>
                                            </div>
                                        </div>
                                        <!--/.Card image-->
                                    </div>
                                    <!--/.Collection card-->

                                </div>
                                <!--/First column-->

                                <!--Second column-->
                                <div class="col-lg-3 col-md-6 mb-r wow fadeIn" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeIn;">

                                    <!--Collection card-->
                                    <div class="card collection-card z-depth-1-half">
                                        <!--Card image-->
                                        <div class="view  hm-zoom">
                                            <img src="http://mdbootstrap.com/images/ecommerce/vertical/img%20(7).jpg" class="img-fluid" alt="">
                                            <div class="stripe light black-text">
                                                <a>
                                                    <p> <h3>Alisado Total</h3></p>
                                                </a>
                                            </div>
                                        </div>
                                        <!--/.Card image-->
                                    </div>
                                    <!--/.Collection card-->

                                </div>
                                <!--/Second column-->

                                <!--Third column-->
                                <div class="col-lg-3 col-md-6 mb-r wow fadeIn" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeIn;">

                                    <!--Collection card-->
                                    <div class="card collection-card z-depth-1-half">
                                        <!--Card image-->
                                        <div class="view  hm-zoom">
                                            <img src="http://mdbootstrap.com/images/ecommerce/vertical/img%20(10).jpg" class="img-fluid" alt="">
                                            <div class="stripe dark white-text">
                                                <a>
                                                    <p> <h3>Rulos Definidos</h3></p>
                                                </a>
                                            </div>
                                        </div>
                                        <!--/.Card image-->
                                    </div>
                                    <!--/.Collection card-->

                                </div>
                                <!--/Third column-->

                                <!--Fourth column-->
                                <div class="col-lg-3 col-md-6 mb-r wow fadeIn" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeIn;">

                                    <!--Collection card-->
                                    <div class="card collection-card z-depth-1-half">
                                        <!--Card image-->
                                        <div class="view  hm-zoom">
                                            <img src="http://mdbootstrap.com/images/ecommerce/vertical/img%20(12).jpg" class="img-fluid" alt="">
                                            <div class="stripe light black-text ">
                                                <a>
                                                    <p> <h3> Cabellos Grasos</h3></p>
                                                </a>
                                            </div>
                                        </div>
                                        <!--/.Card image-->
                                    </div>
                                    <!--/.Collection card-->

                                </div>
                                <!--/Fourth column-->

                            </div>
                            <!--/First row-->

                        </section>

                    </div>
                </div>

                <script type="text/javascript">

                    function productosProfesionales(){
                        var producto = "profesionales";
                        $.ajax({
                            data:  { "producto" : producto}, 
                            url:   'controller.php',
                            type:  'post',
                            success:  function (response) {
                                $("#productos").html(response);
                                console.log(response);
                            }
                        });
                    }
                </script>

                <?php
                include_once("layout/footer.php");
                ?>